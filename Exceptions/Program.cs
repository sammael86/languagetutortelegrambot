﻿using System;
using System.IO;

namespace Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var r = GetRowCount("c:/sdffd/sdfsdf/ddd.txt");
                Console.WriteLine(r);
            }
            catch (AppException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static int GetRowCount(string path)
        {
            try
            {
                var rows = File.ReadAllLines(path);
                return rows.Length;
            }
            catch (FileNotFoundException ex)
            {
                //Console.WriteLine($"Файл {path} не найден!");
                Console.WriteLine(ex.Message);
                return 0;
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("Функция была вызвана с пустым аргументом.");
                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error!");
                var appEx = new AppException();
                appEx.MethodName = "GetRowCount";
                throw appEx;
            }
        }
    }
}
