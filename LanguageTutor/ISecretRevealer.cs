﻿namespace LanguageTutor
{
    public interface ISecretRevealer
    {
        string Reveal();
    }
}