﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using Telegram.Bot;

namespace LanguageTutor
{
    public class Program
    {
        public static IConfigurationRoot Configuration { get; set; }

        static TelegramBotClient Bot;
        static Tutor Tutor = new Tutor();
        static Dictionary<int, string> LastWord = new Dictionary<int, string>();
        const string COMMAND_LIST =
@"Список команд:
/add <eng> <rus> - добавление английского слова и его перевода в словарь
/get - получаем случайное английское слово из словаря
/check <eng> <rus> - проверяем правильность перевода английского слова
";
        static void Main(string[] args)
        {
            var devEnvironmentVariable = Environment.GetEnvironmentVariable("NETCORE_ENVIRONMENT");
            var isDevelopment = string.IsNullOrEmpty(devEnvironmentVariable) ||
                                devEnvironmentVariable.ToLower() == "development";
            var builder = new ConfigurationBuilder();
            builder.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            if (isDevelopment)
                builder.AddUserSecrets<Program>();

            Configuration = builder.Build();

            IServiceCollection services = new ServiceCollection();

            services
                .Configure<TelegramAPI>(Configuration.GetSection(nameof(TelegramAPI)))
                .AddOptions()
                .AddSingleton<ISecretRevealer, SecretRevealer>()
                .BuildServiceProvider();

            var serviceProvider = services.BuildServiceProvider();

            var revealer = serviceProvider.GetService<ISecretRevealer>();

            Bot = new TelegramBotClient(revealer.Reveal());

            Bot.OnMessage += Bot_OnMessage;
            Bot.StartReceiving();
            Console.ReadLine();
            Bot.StopReceiving();
        }

        private static async void Bot_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            if (e == null || e.Message == null || e.Message.Type != Telegram.Bot.Types.Enums.MessageType.Text)
                return;

            var userId = e.Message.From.Id;
            var msgArgs = e.Message.Text.Split(' ');
            string text;
            switch (msgArgs[0])
            {
                case "/start":
                    text = COMMAND_LIST;
                    break;
                case "/add":
                    text = AddWords(msgArgs);
                    break;
                case "/get":
                    text = GetRandomEngWord(userId);
                    break;
                case "/check":
                    text = CheckWord(msgArgs);
                    var newWord = GetRandomEngWord(userId);
                    text += $"\r\nСледующее слово: {newWord}";
                    break;
                default:
                    if (LastWord.ContainsKey(userId))
                    {
                        text = CheckWord(LastWord[userId], msgArgs[0]);
                        newWord = GetRandomEngWord(userId);
                        text += $"\r\nСледующее слово: {newWord}";
                    }
                    else
                        text = COMMAND_LIST;
                    break;
            }
            await Bot.SendTextMessageAsync(userId, text);
        }

        private static string GetRandomEngWord(int userId)
        {
            var text = Tutor.GetRandomEngWord();
            if (LastWord.ContainsKey(userId))
                LastWord[userId] = text;
            else
                LastWord.Add(userId, text);
            return text;
        }

        private static string CheckWord(string[] msgArgs)
        {
            if (msgArgs.Length != 3)
                return "Неправильное количество аргументов. Их должно быть 2";
            else
                return CheckWord(msgArgs[1], msgArgs[2]);
        }

        private static string CheckWord(string eng, string rus)
        {
            if (Tutor.CheckWord(eng, rus))
                return "Правильно!";
            else
            {
                var answer = Tutor.Translate(eng);
                return $"Неверно! Правильный ответ: \"{answer}\".";
            }
        }

        private static string AddWords(string[] msgArgs)
        {
            if (msgArgs.Length != 3)
                return "Неправильное количество аргументов. Их должно быть 2";
            else
            {
                Tutor.AddWord(msgArgs[1], msgArgs[2]);
                return "Новое слово добавлено в словарь";
            }
        }
    }
}