﻿using Microsoft.Extensions.Options;
using System;

namespace LanguageTutor
{
    public class SecretRevealer : ISecretRevealer
    {
        private readonly TelegramAPI _secrets;

        public SecretRevealer(IOptions<TelegramAPI> secrets)
        {
            _secrets = secrets.Value ?? throw new ArgumentNullException(nameof(secrets));
        }

        public string Reveal()
        {
            return _secrets.Token;
        }
    }
}